import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gantt-kendo',
  templateUrl: './gantt-kendo.component.html',
  styleUrls: ['./gantt-kendo.component.scss'],
})
export class GanttKendoComponent implements OnInit {
  public selectedKeys: number[] = [];
  public data = [
    {
      id: 7,
      title: 'Software validation, research and implementation',
      start: new Date('2022-06-02T00:00:00.000Z'),
      end: new Date('2022-06-15T00:00:00.000Z'),
      // completionRatio: 0.45708333333333334,
    },
    {
      id: 8,
      title: 'Project Kickoff',
      start: new Date('2022-06-13T00:00:00.000Z'),
      end: new Date('2022-06-27T00:00:00.000Z'),
      // completionRatio: 0.45708333333333334,
    },
    {
      id: 9,
      title: 'Research',
      start: new Date('2022-07-15T00:00:00.000Z'),
      end: new Date('2022-07-22T00:00:00.000Z'),
      // completionRatio: 0.45708333333333334,
    },
  ];
  constructor() {}

  ngOnInit(): void {}
}
