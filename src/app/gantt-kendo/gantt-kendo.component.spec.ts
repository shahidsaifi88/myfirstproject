import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GanttKendoComponent } from './gantt-kendo.component';

describe('GanttKendoComponent', () => {
  let component: GanttKendoComponent;
  let fixture: ComponentFixture<GanttKendoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GanttKendoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GanttKendoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
