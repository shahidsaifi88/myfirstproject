import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ClientFormService {
  baseUrl = 'https://jsonplaceholder.typicode.com/';
  constructor(public http: HttpClient) {}

  getIndustriesApi() {
    return this.genericMapData(`users`, 'name');
  }
  getSectorApi() {
    return this.genericMapData(`users`, 'username');
  }
  getCountryApi() {
    return this.genericMapData(`users`, 'email');
  }

  sendEmailData(data: any) {
    // console.log(data);
    return of({ status: 200 });
    // return this.http.post(`${this.baseUrl}posts`, data, {
    //   observe: 'response',
    // });
  }

  genericMapData(url: string, key: string) {
    return this.http
      .get<any>(`${this.baseUrl}${url}`)
      .pipe(map((res: any) => res.map((val: any) => val[key])));
  }
}
