import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ClientFormService } from './client-form.service';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

describe('ClientFormService', () => {
  let service: ClientFormService;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(ClientFormService);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should be invoke getIndustriesApi', () => {
    service.getIndustriesApi();
    expect(true).toBeTruthy();
  });
  it('should be invoke genericMapData', () => {
    const data = [{}];
    const spyData = spyOn(httpClient, 'get').and.returnValue(of(data));
    service.getIndustriesApi().subscribe();
    expect(spyData).toHaveBeenCalled();
  });
  it('should be invoke getSectorApi', () => {
    service.getSectorApi();
    expect(true).toBeTruthy();
  });
  it('should be invoke getCountryApi', () => {
    service.getCountryApi();
    expect(true).toBeTruthy();
  });
  it('should be invoke sendEmailData', () => {
    service.sendEmailData({ name: 'shahid' });
    expect(true).toBeTruthy();
  });
});
