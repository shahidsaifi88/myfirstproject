import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { ClientFormComponent } from './client-form.component';
import { ClientFormService } from './client-form.service';

describe('ClientFormComponent', () => {
  let component: ClientFormComponent;
  let fixture: ComponentFixture<ClientFormComponent>;
  let userService: ClientFormService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClientFormComponent],
      imports: [HttpClientTestingModule, FormsModule],
      providers: [ClientFormService],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientFormComponent);
    userService = TestBed.inject(ClientFormService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should invoke onSubmit method', () => {
    component.onSubmit();
    expect(component).toBeTruthy();
  });
  it('should call getIndustries and return list of industries', () => {
    const response: any[] = [];
    const spydata = spyOn(userService, 'getIndustriesApi').and.returnValue(
      of(response)
    );
    component.getIndustries();
    fixture.detectChanges();
    expect(spydata).toHaveBeenCalled();
  });
  it('should call getSectors and return list of sector', () => {
    const response: any[] = [];
    const spydata = spyOn(userService, 'getSectorApi').and.returnValue(
      of(response)
    );
    component.getSectors();
    fixture.detectChanges();
    expect(spydata).toHaveBeenCalled();
  });
  it('should call getCountries and return list of countries', () => {
    const response: any[] = [];
    const spydata = spyOn(userService, 'getCountryApi').and.returnValue(
      of(response)
    );
    component.getCountries();
    fixture.detectChanges();
    expect(spydata).toHaveBeenCalled();
  });
});
