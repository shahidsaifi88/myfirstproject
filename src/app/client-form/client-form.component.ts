import { Component, OnInit } from '@angular/core';
import { ClientFormService } from './client-form.service';

@Component({
  selector: 'app-client-form',
  templateUrl: './client-form.component.html',
  styleUrls: ['./client-form.component.scss'],
})
export class ClientFormComponent implements OnInit {
  successBanner = false;
  clientForm = { firstName: '', lastName: '', password: '' };
  industries = [];
  sectors = [];
  countries = [];

  constructor(private clientService: ClientFormService) {}

  ngOnInit(): void {
    this.getIndustries();
    this.getSectors();
    this.getCountries();
  }

  onSubmit() {
    this.clientService.sendEmailData(this.clientForm).subscribe((res) => {
      if (res.status == 200) {
        this.successBanner = true;
        this.clientForm = { firstName: '', lastName: '', password: '' };
      }
    });
  }

  getIndustries() {
    this.industries = [];
    this.clientService
      .getIndustriesApi()
      .subscribe((res) => (this.industries = res));
  }
  getSectors() {
    this.industries = [];
    this.clientService
      .getSectorApi()
      .subscribe((res) => (this.industries = res));
  }
  getCountries() {
    this.countries = [];
    this.clientService
      .getCountryApi()
      .subscribe((res) => (this.countries = res));
  }
}
